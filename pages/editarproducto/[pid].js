import React from 'react';
import {Formik} from "formik";
import Layout from "../../components/Layout";
import ProductForm from "../../components/ProductsComponents/ProductForm";
import {useRouter} from "next/router";
import {useMutation, useQuery} from "@apollo/client";

import obtenerProducto from "../../graphql/products/obtenerProducto";
import * as Yup from "yup";
import actualizarProducto from "../../graphql/products/actualizarProducto";
import ShowSuccesAlert from "../../components/Utils/ShowSuccesAlert";


const OBTENER_PRODUCTO = obtenerProducto
const ACTUALIZAR_PRODUCTO = actualizarProducto
const EditarProducto = () => {
    const router = useRouter();
    const [actualizarProducto] = useMutation(ACTUALIZAR_PRODUCTO);
    const {query: {id}} = router;
    //consultar obtener Producto
    const {data, loading, error} = useQuery(OBTENER_PRODUCTO, {
        variables: {
            id
        }
    })
    if(loading)return ( <Layout>
        <p>Cargando</p>
    </Layout>)
    const { obtenerProducto } = data
    const validationSchema = Yup.object({
        nombre: Yup.string().required("El nombre es obligatorio"),
        existencia: Yup.number().required("La existencia es obligatoria").min(1, 'La cantidad debe ser mayor a 0'),
        precio: Yup.number().required("El precio es obligatorio").min(0.01, 'El precio debe ser mayor a 0'),
    })
    const actualizarInfoProducto = async (values)=>{
        console.log('ID', id)
        const {nombre ,existencia, precio } = values;
        try {
            const {data} = await actualizarProducto({
                variables:{
                    id:id,
                    input:{
                        nombre,
                        existencia,
                        precio
                    }
                }
            });
            router.push('/');
            ShowSuccesAlert('Producto Actualizado', 'success', `Se actualizó el producto ${nombre} correctamente` )
        }catch (err){
            console.log(err)
            ShowSuccesAlert('Error', 'error', err.message);
        }

    }


    return (
        <Layout>
            {loading ? 'Cargando' :
                <>
                <h1 className="text-2xl text-gray-800 font-light">Editar Producto</h1>
                <div className="flex justify-center mt-5">
                <div className="w-full max-w-lg">
                <Formik validationSchema={validationSchema} enableReinitialize initialValues={obtenerProducto}
                onSubmit={(values) => {
                    actualizarInfoProducto(values)
            }}>
                {props => {
                    return (
                        <ProductForm props={props} isEdit={true}/>
                    )
                }}
                </Formik>

                </div>
                </div>
                </>
            }
        </Layout>
    )
}
export default EditarProducto;