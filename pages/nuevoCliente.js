import React, {useState} from "react";
import Layout from "../components/Layout";
import * as Yup from "yup";
import {useMutation} from '@apollo/client'
import {useRouter} from 'next/router';
import Toast from '../components/Toast';
import {Formik} from "formik";
import ClientForm from "../components/ClientsComponents/ClientForm";
import obtenerClientesVendedor from "../graphql/clients/obtenerClientesVendedor";
import nuevoCliente from "../graphql/clients/nuevoCliente";

const OBTENER_CLIENTES_USUARIO = obtenerClientesVendedor;
const NUEVO_CLIENTE = nuevoCliente;

const NuevoCliente = () => {
    const [mensaje, guardarMensaje] = useState(null);
    const [tipoMensaje, guardarTipoMensaje] = useState('');
    const [nuevoCliente] = useMutation(NUEVO_CLIENTE, {
        update(cache, {data: {nuevoCliente}}) {
            const {obtenerClientesVendedor} = cache.readQuery({
                query: OBTENER_CLIENTES_USUARIO
            });
            cache.writeQuery({
                query: OBTENER_CLIENTES_USUARIO,
                data: {
                    obtenerClientesVendedor: [...obtenerClientesVendedor, nuevoCliente]
                }
            })
        }
    })
    const router = useRouter();
    const validationSchema = Yup.object({
        nombre: Yup.string().required("El nombre es obligatorio"),
        apellido: Yup.string().required("El apellido es obligatorio"),
        empresa: Yup.string().required("La empresa es obligatoria"),
        email: Yup.string()
            .email("El email no tiene el formato correcto")
            .required("El email es obligatorio"),
        telefono: Yup.string().required("El teléfono es obligatorio").max(10, 'El teéfono debe tener máximo 10 dígitos'),
    })

    const crearCliente = async (valores) => {
        const {nombre, apellido, empresa, email, telefono} = valores;
        try {
            const {data} = await nuevoCliente({
                variables: {
                    input: {
                        nombre,
                        apellido,
                        empresa,
                        email,
                        telefono
                    }
                }
            })
            router.push('/')
        } catch (error) {
            guardarTipoMensaje('alerta')
            guardarMensaje(error.message.replace('GraphQL error: ', ''));
            setTimeout(() => {
                guardarMensaje(null)
            }, 3000)
        }
    }

    const mostrarMensaje = (tipoMensaje) => {
        return (
            <Toast mensaje={mensaje} tipoMensaje={tipoMensaje}></Toast>
        )
    }
    const initialValuesForm = () => ({
            nombre: '',
            apellido: '',
            email: '',
            telefono: '',
            empresa: ''
        }
    )


    return (
        <Layout>
            {mensaje && mostrarMensaje(tipoMensaje)}
            <h1 className="text-2xl text-gray-800 font-light">Nuevo Cliente</h1>
            <div className="flex justify-center mt-5">
                <div className="w-full max-w-lg">
                    <Formik initialValues={
                        initialValuesForm()

                    } onSubmit={values => {
                        crearCliente(values);
                    }} validationSchema={validationSchema}>
                        {props => {
                            return (
                                <ClientForm props={props} isEdit={false}/>
                            )
                        }}
                    </Formik>
                </div>
            </div>
        </Layout>
    );
};

export default NuevoCliente;
