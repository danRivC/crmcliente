import React, {useState} from 'react';
import Layout from '../components/Layout';
import {useRouter} from 'next/router';
import {Formik} from 'formik'
import * as Yup from 'yup';
import {gql, useMutation} from '@apollo/client';
import Toast from '../components/Toast';
import Loading from '../components/Loading'
import autenticarUsuario from "../graphql/user/autenticarUsuario";
import FormLogin from "../components/LoginComponents/FormLogin";

const AUTENTICAR_USUARIO = autenticarUsuario;

const Login = () => {
    const [mensaje, guardarMensaje] = useState(null);
    const [tipoMensaje, guardarTipoMensaje] = useState('')
    //Mutation para autenticar usuarios
    const [autenticarUsuario] = useMutation(AUTENTICAR_USUARIO);
    //hook routing
    const router = useRouter();
    const initialValues = () => ({
        email: '',
        password: ''
    })
    const login = async (variables) => {
        try {
            const {data} = await autenticarUsuario({
                variables: {
                    input: {
                        email: variables.email,
                        password: variables.password
                    }
                }
            });
            localStorage.setItem('token', data.autenticarUsuario.token)
            setTimeout(() => {
                router.push('/')

            }, 1000)
        } catch (error) {
            guardarTipoMensaje('alerta')
            guardarMensaje(error.message.replace('GraphQL error: ', ''));
            setTimeout(() => {
                guardarMensaje(null)
            }, 3000)
        }
    }
    const validationSchema = Yup.object({
        email: Yup.string().email('El email no tiene el formato correcto').required('El email es requerído'),
        password: Yup.string().required('El password es requerído').min(6, 'El password tiene mínimo 6 caracteres')
    })
    const mostrarMensaje = (tipoMensaje) => {
        return (
            <>
                <Toast mensaje={mensaje} tipoMensaje={tipoMensaje}></Toast>
                <Loading/>
            </>
        )
    }
    return (
        <>
            <Layout>
                {mensaje && mostrarMensaje(tipoMensaje)}
                <h1 className="text-center text-2xl text-white font-light">Login</h1>
                <div className="flex justify-center mt-5">
                    <div className="w-full max-w-sm">
                        <Formik initialValues={initialValues()} onSubmit={values => login(values)} validationSchema={validationSchema}>
                            {props => {
                                return (
                                    <FormLogin formik={props}></FormLogin>
                                )
                            }}
                        </Formik>
                    </div>
                </div>
            </Layout>
        </>
    );
}

export default Login;