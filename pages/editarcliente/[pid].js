import React from 'react';
import {useRouter} from 'next/router'
import Layout from "../../components/Layout";
import {useQuery, gql, useMutation} from "@apollo/client";
import {Formik} from "formik";
import ClientForm from "../../components/ClientsComponents/ClientForm";
import * as Yup from "yup";
import obtenerCliente from "../../graphql/clients/obtenerCliente";
import actualizarCliente from "../../graphql/clients/actualizarCliente";

const OBTENER_CLIENTE = obtenerCliente
const ACTUALIZAR_CLIENTE = actualizarCliente

const EditarCliente = () => {
    const router = useRouter();
    const {query: {id}} = router;
    //consultar obtener Cliente
    const {data, loading, error} = useQuery(OBTENER_CLIENTE, {
        variables: {
            id
        }
    })
    //Actualizar Cliente
    const [actualizarCliente] = useMutation(ACTUALIZAR_CLIENTE)

    if (loading) return 'Cargando...'
    //Schema Validation

    const validationSchema = Yup.object({
        nombre: Yup.string().required("El nombre es obligatorio"),
        apellido: Yup.string().required("El apellido es obligatorio"),
        empresa: Yup.string().required("La empresa es obligatoria"),
        email: Yup.string()
            .email("El email no tiene el formato correcto")
            .required("El email es obligatorio"),
        telefono: Yup.string().required("El teléfono es obligatorio").max(10, 'El teéfono debe tener máximo 10 dígitos'),
    })

    const actualizarInfoCliente = async (values) => {
        const {nombre ,apellido, empresa, email, telefono, } = values;
        try {
            const {data} = await actualizarCliente({
                variables:{
                    id:id,
                    input:{
                        nombre,
                        apellido,
                        empresa,
                        email,
                        telefono
                    }
                }
            });
            router.push('/');
        }catch (err){
            console.log(err)
        }
    }

    const {obtenerCliente} = data;
    return (
        <Layout>
            <h1 className="text-2xl text-gray-800 font-light">Editar Cliente</h1>
            <div className="flex justify-center mt-5">
                <div className="w-full max-w-lg">
                    <Formik validationSchema={validationSchema} enableReinitialize initialValues={obtenerCliente}
                            onSubmit={(values) => {
                                actualizarInfoCliente(values)
                            }}>
                        {props => {
                            return (
                                <ClientForm props={props} isEdit={true}/>
                            )
                        }}
                    </Formik>

                </div>
            </div>
        </Layout>
    );
}
export default EditarCliente;