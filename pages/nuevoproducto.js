import React from 'react';
import Layout from "../components/Layout";
import {Formik} from "formik";
import {useMutation} from "@apollo/client";
import ProductForm from "../components/ProductsComponents/ProductForm";
import nuevoProducto from "../graphql/products/nuevoProducto";
import obtenerProductos from "../graphql/products/obtenerProductos";
import * as Yup from "yup";
import ShowSuccesAlert from "../components/Utils/ShowSuccesAlert";
import {useRouter} from "next/router";

const NUEVO_PRODUCTO = nuevoProducto;
const OBTENER_PRODUCTOS = obtenerProductos;

const NuevoProducto = () => {
    const router = useRouter();
    const [nuevoProducto] = useMutation(NUEVO_PRODUCTO, {
        update(cache, {data: {nuevoProducto}}) {
            const {obtenerProductos} = cache.readQuery({
                query: OBTENER_PRODUCTOS
            });
            cache.writeQuery({
                query: OBTENER_PRODUCTOS,
                data: {
                    obtenerProductos: [...obtenerProductos, nuevoProducto]
                }
            })
        }
    })

    const initialValuesForm = () => ({
            nombre: '',
            existencia: '',
            precio: ''

        }
    )
    const validationSchema = Yup.object({
        nombre: Yup.string().required("El nombre es obligatorio"),
        existencia: Yup.number().required("La existencia es obligatoria").min(1, 'La cantidad debe ser mayor a 0'),
        precio: Yup.number().required("El precio es obligatorio").min(0.01, 'El precio debe ser mayor a 0'),
    })

    const crearProducto = async (values)=>{
        const {nombre, existencia, precio} = values;
        try {
            const {data} = await nuevoProducto({
                variables: {
                    input: {
                        nombre,
                        existencia,
                        precio
                    }
                }
            });
            router.push('/productos');
            ShowSuccesAlert('Producto Agregado', 'success', `Se agregó ${existencia} ${nombre} correctamente`)
        }catch (err){
            console.log(err)
            ShowSuccesAlert('Ocurrio un error', 'error', err)
        }
    }
    return (
        <Layout>
            <h1 className="text-2xl text-gray-800 font-light">Nuevo Producto</h1>
            <div className="flex justify-center mt-5">
                <div className="w-full max-w-lg">
                    <Formik initialValues={
                        initialValuesForm()

                    } onSubmit={values => {
                        crearProducto(values);
                    }} validationSchema={validationSchema}>
                        {props => {
                            return (
                                <ProductForm props={props} isEdit={false}></ProductForm>
                            )
                        }}
                    </Formik>
                </div>
            </div>
        </Layout>
    )
}
export default NuevoProducto;