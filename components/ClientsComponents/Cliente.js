import React from 'react';
import {gql, useMutation} from '@apollo/client';
import Router from 'next/router';
import {FiEdit, FiTrash2} from "react-icons/fi";
import obtenerClientesVendedor from "../../graphql/clients/obtenerClientesVendedor";
import eliminarCliente from "../../graphql/clients/eliminarCliente";
import ShowConfirmAlert from "../Utils/ShowConfirmAlert";
import ShowSuccesAlert from "../Utils/ShowSuccesAlert";

const ELIMINAR_CLIENTE = eliminarCliente;
const OBTENER_CLIENTES_USUARIO = obtenerClientesVendedor


const Cliente = ({cliente}) => {
    const [eliminarCliente] = useMutation(ELIMINAR_CLIENTE, {
        update(cache) {
            const {obtenerClientesVendedor} = cache.readQuery({query: OBTENER_CLIENTES_USUARIO});
            cache.writeQuery({
                query: OBTENER_CLIENTES_USUARIO,
                data: {
                    obtenerClientesVendedor: obtenerClientesVendedor.filter(clienteActual => clienteActual.id !== id)
                }
            })
        }
    });
    const {id, nombre, apellido, empresa, email} = cliente
    const confirmarEliminarCliente = (id) => {
        ShowConfirmAlert('warning',
            'Esta acciòn no se puede deshacer',
            'Esta seguro de eliminar a este cliente?',
            'No, cancelar',
            'Si, eliminar').then(async (result) => {
            if (result.value) {
                try {
                    const {data} = await eliminarCliente({
                        variables: {
                            id
                        }
                    })
                    ShowSuccesAlert('Eliminado!','success', data.eliminarCliente)
                } catch (error) {
                    console.log(error)
                }
            }
        })
    }
    const editarCliente = () => {

        Router.push({
            pathname: '/editarcliente/[id]',
            query: {id}
        })
    }

    return (

        <tr>
            <td className="border px-4 py-2">{nombre} {apellido}</td>
            <td className="border px-4 py-2">{empresa} </td>
            <td className="border px-4 py-2">{email} </td>
            <td className='px-12'>
                <button
                    type='button'
                    className='flex justify-center items-center bg-red-800 py-2 px-auto w-full text-white rounded text-xs uppercase font-bold hover:bg-red-600 '
                    onClick={() => confirmarEliminarCliente(id)}
                >
                    Eliminar
                    <span className='mx-2 text-lg'>
                        <i><FiTrash2/></i>
                    </span>
                </button>
            </td>
            <td className='px-12'>
                <button
                    type='button'
                    className='flex justify-center items-center bg-green-600 py-2 px-4 w-full text-white rounded text-xs uppercase font-bold hover:bg-green-500 '
                    onClick={() => editarCliente()}
                >
                    Editar
                    <span className='mx-2 text-lg'>
                        <i><FiEdit/></i>
                    </span>

                </button>
            </td>
        </tr>

    );
}

export default Cliente;