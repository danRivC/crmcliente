import {Formik} from "formik";
import React from 'react';

const ProductForm = ({props, isEdit}) => {
    return (
        <form className="bg-white shadow-md px-8 pt-6 pb-8 mb-4"
              onSubmit={props.handleSubmit}>
            <div className="mb-4">
                <label
                    className="block text-gray-700 text-sm font-bold mb-2"
                    htmlFor="nombre"
                >
                    Nombre
                </label>
                <input
                    className="shadow appearence-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    id="nombre"
                    type="text"
                    placeholder="Nombre del Producto"
                    value={props.values.nombre}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                />
            </div>
            {props.touched.nombre && props.errors.nombre ? (
                <div className="my-2 bg-red-100 border-l-4 border-red-500 text-red-700 p-4">
                    <p className='font-bold'>Error</p>
                    <p>{props.errors.nombre}</p>
                </div>
            ) : null}
            <div className="mb-4">
                <label
                    className="block text-gray-700 text-sm font-bold mb-2"
                    htmlFor="existencia"
                >
                    Existencia
                </label>
                <input
                    className="shadow appearence-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    id="existencia"
                    type="number"
                    placeholder="Cantidad en existencia"
                    value={props.values.existencia}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                />
            </div>
            {props.touched.existencia && props.errors.existencia ? (
                <div className="my-2 bg-red-100 border-l-4 border-red-500 text-red-700 p-4">
                    <p className='font-bold'>Error</p>
                    <p>{props.errors.existencia}</p>
                </div>
            ) : null}
            <div className="mb-4">
                <label
                    className="block text-gray-700 text-sm font-bold mb-2"
                    htmlFor="precio"
                >
                    Precio
                </label>
                <input
                    className="shadow appearence-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    id="precio"
                    type="number"
                    placeholder="Precio unitario"
                    value={props.values.precio}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                />
            </div>
            {props.touched.precio && props.errors.precio ? (
                <div className="my-2 bg-red-100 border-l-4 border-red-500 text-red-700 p-4">
                    <p className='font-bold'>Error</p>
                    <p>{props.errors.precio}</p>
                </div>
            ) : null}
            <input
                type="submit"
                className="bg-gray-800 w-full mt-5 p-2 text-white uppercase font-bold hover:bg-gray-900"
                value={isEdit ? "Actualizar Producto" : 'Ingresar Producto'}
            />
        </form>
    )
}
export default ProductForm;