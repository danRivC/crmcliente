import React, {Fragment} from 'react'
import eliminarProducto from "../../graphql/products/eliminarProducto";
import {useMutation} from "@apollo/client";
import obtenerProductos from "../../graphql/products/obtenerProductos";
import ShowConfirmAlert from "../Utils/ShowConfirmAlert";
import ShowSuccesAlert from "../Utils/ShowSuccesAlert";
import Router from "next/router";

const ELIMINAR_PRODUCTO = eliminarProducto;
const OBTENER_PRODUCTOS = obtenerProductos
const Producto = ({producto}) => {
    const {id, nombre, existencia, precio, creado} = producto
    const [eliminarProducto] = useMutation(ELIMINAR_PRODUCTO, {
        update(cache) {
            const {obtenerProductos} = cache.readQuery({query: OBTENER_PRODUCTOS});
            cache.writeQuery({
                query: OBTENER_PRODUCTOS,
                data: {
                    obtenerProductos: obtenerProductos.filter(productoActual => productoActual.id !== id)
                }
            })
        }
    })
    const confirmarAlerta = ShowConfirmAlert;
    const alertaSuccess = ShowSuccesAlert;
    const confirmarEliminarProducto = (id) => {
        confirmarAlerta(
            'warning',
            'Está seguro de eliminar el producto',
            'Esta acción no se puede revertir',
            'Cancelar',
            'Eliminar')
            .then(async ({isConfirmed}) => {
                if (isConfirmed) {
                    const {data} = await eliminarProducto({
                        variables: {
                            id
                        }
                    })
                    alertaSuccess('Eliminado!', 'success', data.eliminarProducto)
                }
            })
    }
    const editarProducto=(id)=>{
        Router.push({
            pathname: '/editarproducto/[id]',
            query: {id}
        })
    }
    return (
        <Fragment>
            <tr>
                <td className='border px-4 py-2'>
                    {nombre}
                </td>
                <td className='border px-4 py-2'>
                    {existencia}
                </td>
                <td className='border px-4 py-2'>
                    {precio}$
                </td>
                <td className='border px-4 py-2'>
                    <button
                        type='button'
                        onClick={() => confirmarEliminarProducto(id)}
                        className='flex justify-center items-center bg-red-800 py-2 px-auto w-full text-white rounded text-xs uppercase font-bold hover:bg-red-600 '>
                        Eliminar
                    </button>
                </td>
                <td className='border px-4 py-2'>
                    <button
                        type='button'
                        onClick={()=>editarProducto(id)}
                        className='flex justify-center items-center bg-green-600 py-2 px-4 w-full text-white rounded text-xs uppercase font-bold hover:bg-green-500 '>
                        Editar
                    </button>
                </td>
            </tr>
        </Fragment>

    )
}
export default Producto