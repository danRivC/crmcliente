import React, {useState} from 'react';

const Toast = (props) => {
    
    const {mensaje, tipoMensaje} = props

    

    const componente = ()=>{
        if(tipoMensaje === 'alerta'){
            return(
            <div className="absolute pt-6 pb-6 pr-3 pl-3 top-0 right-0 mt-1 mr-1 bg-red-600 text-gray-200">
                {mensaje}
            </div>
        )
        }
        if(tipoMensaje === 'success'){
            return(
            <div className="absolute pt-6 pb-6 pr-3 pl-3 top-0 right-0 mt-1 mr-1 bg-green-600 text-gray-200">
                {mensaje}
            </div>
        )
        }
    }


    return ( 
        <>
            {componente()}        
        </>
     );
}
 
export default Toast;
