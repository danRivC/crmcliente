import Swal from "sweetalert2";

const ShowConfirmAlert= (icon, text, title, cancelButtonText, confirmButtonText)=>{
    return  Swal.fire({
        title: title,
        text: text,
        icon: icon,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText
    })
}
export default ShowConfirmAlert;