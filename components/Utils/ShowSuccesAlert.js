import Swal from "sweetalert2";
const ShowSuccesAlert =(title, icon, text)=>{
    return Swal.fire(
        title,
        text,
        icon
    )

}
export default ShowSuccesAlert;