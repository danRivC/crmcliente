import React from 'react';
import {gql, useQuery} from '@apollo/client'
import {useRouter} from 'next/router'

const OBTENER_USUARIO=gql`
    query obtenerUsuario{
        obtenerUsuario{
            nombre
            apellido
            email
            id
        }
    }

`
const Header = () => {
    //query apollo
    const {data, loading, error}= useQuery(OBTENER_USUARIO);

    const router = useRouter();

    if(loading)return "Cargando";

    

    const {nombre, apellido} = data.obtenerUsuario;

    const cerrarSesion=()=>{
        localStorage.removeItem('token');
        router.push('/login');
    }
    const vistaProtegida=()=>{
        router.push('/login');
      }
    if(!localStorage.getItem('token')){vistaProtegida();} 
    return (
        <>
        {data?
        (
            <div className="flex justify-between mb-10">
            <p className="mr-2">
                Hola:{nombre} {apellido}
            </p>
            <button 
            type="button"
            className="bg-blue-800 w-full sm:w-auto font-bold uppercase text-xs rounded py-1 px-2 text-white shadow-md"
            onClick={()=>cerrarSesion()}
            >Cerrar Sesión</button>

        </div>
        )
        :vistaProtegida()}
        </>  
        
    );
}
 
export default Header;