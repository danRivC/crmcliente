import {gql} from '@apollo/client';
export default gql`
    query obtenerProducto($id:ID!){
        obtenerProducto(id:$id){
            id
            nombre
            existencia
            precio
            creado
        }
    }
`;