import {gql} from '@apollo/client'
export default  gql`
query obtenerProductos{
    obtenerProductos{
        id
        nombre
        existencia
        precio
        creado
    }
}
`