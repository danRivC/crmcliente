import {gql} from '@apollo/client';
export default gql`
    mutation actualizarProducto($id:ID!, $input:ProductoInput!){
        actualizarProducto(id:$id, input:$input){
            id
            nombre
            existencia
            precio
            creado
        }
    }
`