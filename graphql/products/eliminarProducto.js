import {gql} from '@apollo/client';
export default gql`
    mutation eliminarProducto($id:ID!){
        eliminarProducto(id:$id)
    }
`