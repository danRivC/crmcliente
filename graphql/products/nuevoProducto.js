import {gql} from '@apollo/client';

export default gql`
    mutation nuevoProducto($input:ProductoInput){
        nuevoProducto(input:$input){
            id
            nombre
            existencia
            precio
            creado
        }
    }
`