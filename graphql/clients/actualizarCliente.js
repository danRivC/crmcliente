import {gql} from '@apollo/client'
export default gql`
    mutation actualizarCliente($id:ID!, $input:ClienteInput){
        actualizarCliente(id:$id, input:$input){
            id
            nombre
            apellido
            email
            empresa
            telefono
            vendedor
        }
    }
`