import {gql} from '@apollo/client';
export default gql`
    mutation eliminarCliente($id:ID!){
        eliminarCliente(id:$id)
    }
`;