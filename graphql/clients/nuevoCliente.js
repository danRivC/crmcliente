import {gql} from  '@apollo/client';
export default gql`
    mutation nuevoCliente($input:ClienteInput){
        nuevoCliente(input:$input){
            id
            nombre
            apellido
            empresa
            email
            telefono
        }
    }
`;